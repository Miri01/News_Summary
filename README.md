# News_Summary_Seq2Seq_with_.

An encoder decoder LSTM network model with Attention mechanism that is able to provide the appropriate title for a given news article.

Given the limited computational power that I have I only trained the model for 200 epochs and I did not use the 700K articles , I just use 70K articles which give a kind of good result !

To use more articles or train the model further just use the retrain_main.py.